# cedc

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

An NMDC hub client for Windows CE. Binaries supplied for multiple architectures.

The client download contains source code and binaries for WinCE/ARMv4, WinCE/x86 and Win32/x86.

Tags: nmdc


## Download

- [⬇️ cedc-multiarch-2010-06-16.zip](dist-archive/cedc-multiarch-2010-06-16.zip) *(461.26 KiB)*
